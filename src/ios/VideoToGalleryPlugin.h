
#import <Cordova/CDVPlugin.h>

@interface VideoToGalleryPlugin : CDVPlugin
{
	NSString* callbackId;
}

@property (nonatomic, copy) NSString* callbackId;

- (void)saveVideoToGallery:(CDVInvokedUrlCommand*)command;
- (void)saveImageToGallery:(CDVInvokedUrlCommand*)command;

@end
